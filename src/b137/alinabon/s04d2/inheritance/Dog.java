package b137.alinabon.s04d2.inheritance;

public class Dog extends Animal {

    // Properties
    private String breed;

    // Constructor
    public Dog(){
        super();
    }

    public Dog(String name, String color, String breed){
        super(name, color);
        this.breed = breed;
    }
    // Getters and Setters
    public  String getBreed(){
        return breed;
    }

    public void setBreed(String breed) {
        this.breed = breed;
    }

    // Methods
    public void bark(){
        super.showDetail();
        // System.out.println("My breed is " + this.breed);
    }

    public void showDetail(){
        System.out.println("I am " + super.getName() + ", with color " + super.getColor() + ", and breed of " + this.breed);
    }
}
