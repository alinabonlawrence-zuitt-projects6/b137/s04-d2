package b137.alinabon.s04d2.inheritance;

public class Animal {

    // Properties
    private String name;
    private String color;

    public Animal(){}

    public Animal(String name, String color){
        this.name = name;
        this.color = color;
    }

    public String getName(){
        return name;
    }

    public String getColor() {
        return color;
    }

    public void setName(String name) {
        this.name = name;
    }

    public void setColor(String color) {
        this.color = color;
    }

    public void showDetail(){
        System.out.println("I am " + this.name + ", with color " + this.color);
    }
}
